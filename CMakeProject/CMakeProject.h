﻿// CMakeProject.h : Include file for standard system include files,
// or project specific include files.

#pragma once

#include <iostream>
/**
  A fluffy feline
*/
struct cat {
    /**
      Make this cat look super cute
    */
    void make_cute();
};